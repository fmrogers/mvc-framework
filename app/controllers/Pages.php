<?php

class Pages extends Controller
{
	public function __construct() {

	}
	// Index Page
	public function index() {
		$data = [
			'title' => 'Welcome',
		];

		$this->view('pages/index', $data);
	}
	// About Page
	public function about() {
		$data = [
			'title' => 'About Us'
		];
		$this->view('pages/about', $data);
	}
	// Not found
	public function not_found() {
		$this->view('pages/not-found');
	}
}