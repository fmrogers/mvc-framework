<?php
// Database Parameters
define('DB_HOST', 'YOUR_HOST');
define('DB_USER', 'YOUR_USER');
define('DB_PASS', 'YOUR_PASS');
define('DB_NAME', 'YOUR_DB');

// App Root
define('APP_ROOT', dirname(dirname(__FILE__)));
// URL Root
define('URL_ROOT', 'YOUR_URL');
// Site Name
define('SITE_NAME', 'YOUR_SITE_NAME');

