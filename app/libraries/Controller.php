<?php
/**
 * @package     Controller
 * @author      Frederick M. Rogers <fmrogers@carbonium.no>
 * @version     1.0.0
 * @access      public
 * @description Base controller, loads the models and views.
 */
class Controller
{
	// Load model
	public function model($model) {
		// Require model file
		require_once '../app/models/' . $model . '.php';

		// Instantiate model
		return new $model;
	}

	// Load view and model data
	public function view($view, $data = []) {
		// Check for the view file
		if ( file_exists('../app/views/' . $view . '.php') ) {
			// Require view file
			require_once '../app/views/' . $view . '.php';
		} else {
			// View does not exist
			die('View does not exist');
		}
	}
}