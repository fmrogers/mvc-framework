<?php

/**
 * @package     Core
 * @author      Frederick M. Rogers <fmrogers@carbonium.no>
 * @version     1.1.0
 * @access      protected
 * @description Retrieves individual controllers, methods & parameters bases on values of the current URL.
 *              The url format is 'example.com/controller/method/params' (see /public/.htaccess for rewrite rule).
 *
 * @since       1.0.0 - Added string replacement to translate hyphens in url to underscores in controller methods.
 */

class Core
{
	/**
	 * @var string
	 * @description Default CONTROLLER
	 */
	protected $currentController = 'Pages';
	/**
	 * @var string
	 * @description Default METHOD
	 */
	protected $currentMethod = 'not_found';
	/**
	 * @var array
	 * @description Default PARAMETERS
	 */
	protected $params = [];

	/**
	 * Core constructor.
	 */
	public function __construct() {

		$url = $this->getUrl();
		// Search for named controller ('example.com/CONTROLLER/method/params')
		if ( file_exists('../app/controllers/' .  ucwords($url[0] .'.php')) ) {
			// If controller exists, set controller as index 0
			$this->currentController = ucwords($url[0]);
			// Unset index 0 of the $url array
			unset($url[0]);
		}
		// Require the controller
		require_once '../app/controllers/' . $this->currentController . '.php';
		// Instantiate controller class
		$this->currentController = new $this->currentController;
		// Check for second part of url
		if ( isset($url[1]) ) {
			// Replace any hyphens with underscore ie. '-' with '_'.
			$url[1] = str_replace('-','_', $url[1]);
			// Check to see if method exists in controller ('example.com/controller/METHOD/params')
			if ( method_exists($this->currentController, $url[1]) ) {
				$this->currentMethod = $url[1];
			}
			// Unset index 1 of the $url array
			unset($url[1]);
		}
		// Get parameters ('example.com/controller/method/PARAMS')
		$this->params = $url ? array_values($url) : [];
		// Call a callback with array of params
		call_user_func_array([$this->currentController, $this->currentMethod], $this->params);
	}

	/**
	 * @return array
	 * @description Returns an array of values from url separated by '/'
	 */
	public function getUrl() {
		// Check if the URL parameter is set
		if ( isset($_GET['url']) ) {
			$url = rtrim($_GET['url'], '/');
			$url = filter_var($url, FILTER_SANITIZE_URL);
			$url = explode('/', $url);
			return $url;
		}
	}
}

