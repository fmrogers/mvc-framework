# Initial setup
To get started with this MVC framework you need to make some minor changes to the **config.php** located in _**app/config/**_, and then the **.htaccess** file located in the _**views/**_ folder.

### config.php - app/config/
First, you need add the relevant database information to the **config.php** file located in _**app/config/**_ folder. Replace any value starting with "YOUR", with the corresponding database information.

* define('DB_HOST', 'YOUR_HOST');
* define('DB_USER', 'YOUR_USER');
* define('DB_PASS', 'YOUR_PASS');
* define('DB_NAME', 'YOUR_DB');

Second replace the URL_ROOT and SITE_NAME as required.
* define('URL_ROOT', 'YOUR_URL');
* define('SITE_NAME', 'YOUR_SITE_NAME');

### .htaccess - views/
Last but not least you need to make small change to the **.htaccess** file located in _**public/**_. Replace /EDIT_THIS/ with the relevant path to your root folder.
* RewriteBase /EDIT_THIS/public